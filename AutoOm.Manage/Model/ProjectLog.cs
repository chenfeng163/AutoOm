﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model
{
    public class ProjectLog : BaseModel
    {
        [Key,ForeignKey("Project")]
        public new int Id { get; set; }

        public Project Project { get; set; }

        public DateTime BuildTime { get; set; }

        public bool Status { get; set; }

        public List<ProjectTaskLog> Logs { get; set; } 
    }
}