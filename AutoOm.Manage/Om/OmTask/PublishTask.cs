﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AutoOm.Manage.Model;
using AutoOm.Manage.Om.OmInvoke;

namespace AutoOm.Manage.Om.OmTask
{
    public class PublishTask : BaseTask, ITask
    {
        private readonly OmCmd _cmd;
        private readonly string _csprojFile;
        private readonly string _deployName;
        private readonly string _msBuildPath;
        private readonly string _desktopBuildPackageLocation;
        public PublishTask(ProjectConfig config)
        {
            var taskConfig = config as ProjectTaskConfig;
            if (taskConfig == null)
            {
                throw new Exception("taskConfig is null");
            }
            _csprojFile = $"{taskConfig.ProjectDir}//{taskConfig.ProjectName}.csproj";
            _deployName = taskConfig.DeployName;
            _msBuildPath = taskConfig.MsBuildPath;
            _desktopBuildPackageLocation = GetDeployPackagePath(taskConfig.ProjectName);
            _cmd = new OmCmd();
            _cmd.Process += OnProcess;
        }
        public void Start()
        {
            _cmd.Invoke($"\"{_msBuildPath}\" " +
                        $"\"{_csprojFile}\" " +
                        "-v:m " +
                        "/p:DeployOnBuild=true " +
                        $"/p:PublishProfile={_deployName} " +
                        $"/p:DesktopBuildPackageLocation={_desktopBuildPackageLocation}");
            _cmd.Dispose();
        }

        public void Stop()
        {
        }
    }
}