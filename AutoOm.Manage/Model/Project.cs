﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model
{
    public class Project : BaseModel
    {
        public string ProjectName { get; set; }

        public List<ProjectTask> Tasks { get; set; }

        public ProjectTaskConfig ProjectConfig { get; set; }

        public ProjectLog ProjectLog { get; set; }

        public DateTime CreateTime { get; set; }
    }
}