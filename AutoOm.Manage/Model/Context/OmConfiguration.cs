﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model.Context
{
    public class OmConfiguration : DbMigrationsConfiguration<OmDbContext>
    {
        public OmConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(OmDbContext context)
        {

        }
    }
}