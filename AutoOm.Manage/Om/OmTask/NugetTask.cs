﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AutoOm.Manage.Model;
using AutoOm.Manage.Om.OmInvoke;

namespace AutoOm.Manage.Om.OmTask
{
    public class NugetTask : BaseTask, ITask
    {
        private readonly OmCmd _cmd;
        private readonly string _nugetFile;

        public NugetTask(ProjectConfig config)
        {
            var taskConfig = config as ProjectTaskConfig;
            if (taskConfig == null)
            {
                throw new Exception("taskConfig is null");
            }
            _nugetFile = taskConfig.NugetPath;
            _cmd = new OmCmd("cmd", taskConfig.SourceDir);
            _cmd.Process += OnProcess;
        }
        public void Start()
        {
            _cmd.Invoke($"\"{_nugetFile}\" restore");
            _cmd.Dispose();
        }

        public void Stop()
        {
        }
    }
}