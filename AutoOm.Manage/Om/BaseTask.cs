﻿using System;
using System.IO;

namespace AutoOm.Manage.Om
{
    public class BaseTask
    {
        protected event TaskEventHandler Process;

        public virtual void OnProcess(string log)
        {
            Process?.Invoke(log);
        }
        public void RegisterProcess(TaskEventHandler evt)
        {
            Process += evt;
        }

        protected string GetDeployPackagePath(string projectName)
        {
            var dir = $"{AppDomain.CurrentDomain.BaseDirectory}//DeployPackage//{projectName}";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return dir;
        }
    }

    public delegate void TaskEventHandler(string log);
}