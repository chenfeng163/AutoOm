﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model
{
    public class ProjectTask : BaseModel
    {
        public int ProjectId { get; set; }

        public Project Project { get; set; }

        public string TaskType { get; set; }

        public int Order { get; set; }

        public int ProjectTaskDiyId { get; set; }

        public ProjectTaskDiy ProjectTaskDiy { get; set; }
        
        public ProjectTaskLog ProjectTaskLog { get; set; }
    }

    public class TaskType
    {
        public const string Git = "Git";

        public const string Nuget = "Nuget";

        public const string Publish = "Publish";

        public const string WebDeploy = "WebDeploy";
    }
}