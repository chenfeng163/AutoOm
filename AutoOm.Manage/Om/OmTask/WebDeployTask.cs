﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoOm.Manage.Model;
using AutoOm.Manage.Om.OmInvoke;

namespace AutoOm.Manage.Om.OmTask
{
    public class WebDeployTask : BaseTask, ITask
    {
        private readonly string _projName;
        private readonly OmCmd _cmd;

        public WebDeployTask(ProjectConfig config)
        {
            var taskConfig = config as ProjectTaskConfig;
            if (taskConfig == null)
            {
                throw new Exception("taskConfig is null");
            }
            var desktopBuildPackageLocation = GetDeployPackagePath(taskConfig.ProjectName);
            _projName = taskConfig.ProjectName;
            _cmd = new OmCmd("cmd", desktopBuildPackageLocation);
            _cmd.Process += OnProcess;
        }
        public void Start()
        {
            _cmd.Invoke($"{_projName}.deploy.cmd /y");
        }

        public void Stop()
        {

        }
    }
}