﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoOm.Manage.Model;
using AutoOm.Manage.Om;
using AutoOm.Manage.Om.OmProcess;

namespace AutoOm.Manage.Controllers
{
    public class TaskController : ApiController
    {
        public IHttpActionResult Get()
        {
            var project = new Project()
            {
                ProjectName = "Hlj.Manage",
                ProjectConfig = new ProjectTaskConfig()
                {
                    ProjectName = "Hlj.Manage",
                    Auto = false,
                    DeployName = "deployManage",
                    MsBuildPath = "C:/Program Files (x86)//MSBuild//14.0//Bin//MSBuild.exe",
                    NugetPath = "D://nuget.exe",
                    ProjectDir = "D://AutoOm//Hlj.Manage",
                    SourceBranch = "master",
                    SourceDir = "D://AutoOm",
                    SourceUrl = "git@git.oschina.net:chenfeng163/helejia.git"
                },
                Tasks = new List<ProjectTask>()
                {
                    new ProjectTask()
                    {
                        TaskType = TaskType.Git,
                        ProjectTaskLog = new ProjectTaskLog()
                    },
                    new ProjectTask()
                    {
                        TaskType = TaskType.Nuget,
                        ProjectTaskLog = new ProjectTaskLog()
                    },
                    new ProjectTask()
                    {
                        TaskType = TaskType.Publish,
                        ProjectTaskLog = new ProjectTaskLog()
                    },
                    new ProjectTask()
                    {
                        TaskType = TaskType.WebDeploy,
                        ProjectTaskLog = new ProjectTaskLog()
                    },
                },
                ProjectLog = new ProjectLog()
            };
            var process = new ProjectProcess(project);
            AutoOmClient.Start(process);
            return Ok();
        }
    }
}
