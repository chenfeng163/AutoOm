﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model
{
    public class ProjectTaskDiy : ProjectConfig
    {
        [Key, ForeignKey("ProjectTask")]
        public new int Id { get; set; }

        public int ProjectId { get; set; }

        public Project Project { get; set; }

        public ProjectTask ProjectTask { get; set; }

        public string RunPath { get; set; }
    }
}