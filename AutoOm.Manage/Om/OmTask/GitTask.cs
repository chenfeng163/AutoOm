﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using AutoOm.Manage.Model;
using AutoOm.Manage.Om.OmInvoke;

namespace AutoOm.Manage.Om.OmTask
{
    public class GitTask : BaseTask, ITask
    {
        private readonly string _url;
        private readonly string _master;
        private readonly OmCmd _cmd;

        public GitTask(ProjectConfig config)
        {
            var taskConfig = config as ProjectTaskConfig;
            if (taskConfig == null)
            {
                throw new Exception("taskConfig is null");
            }
            _url = taskConfig.SourceUrl;
            _master = taskConfig.SourceBranch;
            _cmd = new OmCmd("cmd", taskConfig.SourceDir);
            _cmd.Process += OnProcess;
        }
        public void Start()
        {
            var result = _cmd.Invoke("git init");
            if (result.Contains("Initialized"))
            {
                _cmd.Invoke($"git remote add origin {_url}");
            }
            _cmd.Invoke($"git pull origin {_master}");
            _cmd.Dispose();
        }

        public void Stop()
        {
        }
    }

}