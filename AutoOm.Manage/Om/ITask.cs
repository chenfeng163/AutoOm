﻿namespace AutoOm.Manage.Om
{
    public interface ITask
    {
        void Start();
        void Stop();
        void OnProcess(string log);
        void RegisterProcess(TaskEventHandler evt);
    }
}