﻿using System.Diagnostics;
using System.Threading.Tasks;
using AutoOm.Manage.Om.OmProcess;

namespace AutoOm.Manage.Om
{
    public class AutoOmClient
    {
        public static void Start(ProjectProcess process)
        {
            Task.Run(() =>
            {
                foreach (var task in process.Tasks)
                {
                    task.Start();
                }
            });
        }
    }
}