﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model
{
    public class ProjectTaskLog : BaseModel
    {
        [Key, ForeignKey("ProjectTask")]
        public new int Id { get; set; }

        public ProjectTask ProjectTask { get; set; }

        public int ProjectId { get; set; }

        public Project Project { get; set; }

        public bool Status { get; set; }

        public string Log { get; set; }
    }
}