﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model
{
    public class ProjectTaskConfig : ProjectConfig
    {
        [Key, ForeignKey("Project")]
        public new int Id { get; set; }

        public Project Project { get; set; }

        public bool Auto { get; set; }

        public string ProjectName { get; set; }

        public string SourceDir { get; set; }

        public string SourceUrl { get; set; }

        public string SourceBranch { get; set; }

        public string NugetPath { get; set; }

        public string ProjectDir { get; set; }

        public string DeployName { get; set; }

        public string MsBuildPath { get; set; }
    }
}