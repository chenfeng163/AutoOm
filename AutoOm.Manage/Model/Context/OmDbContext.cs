﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AutoOm.Manage.Model.Context
{
    public class OmDbContext : DbContext
    {
        public OmDbContext() :
            base("DbConn")
        { }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTaskConfig> ProjectConfigs { get; set; }
        public DbSet<ProjectLog> ProjectLogs { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }
        public DbSet<ProjectTaskDiy> ProjectTaskDiys { get; set; }
        public DbSet<ProjectTaskLog> ProjectTaskLogs { get; set; }
    }
}